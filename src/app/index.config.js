export function config ($logProvider, toastrConfig, $mdThemingProvider) {
  'ngInject';
  // Enable log
  $logProvider.debugEnabled(true);

  // Set options third-party lib
  toastrConfig.allowHtml = true;
  toastrConfig.timeOut = 3000;
  toastrConfig.positionClass = 'toast-top-right';
  toastrConfig.preventDuplicates = true;
  toastrConfig.progressBar = true;

  //set custom templating for angular material
  $mdThemingProvider.definePalette('amazingPaletteName', {
    '50': 'c3d500',
    '100': 'c3d500',
    '200': 'c3d500',
    '300': 'c3d500',
    '400': 'c3d500',
    '500': 'c3d500',
    '600': 'c3d500',
    '700': 'c3d500',
    '800': 'c3d500',
    '900': 'c3d500',
    'A100': 'c3d500',
    'A200': 'c3d500',
    'A400': 'c3d500',
    'A700': 'c3d500',
    'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
                                        // on this palette should be dark or light
    'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
      '200', '300', '400', 'A100'],
    'contrastLightColors': undefined    // could also specify this if default was 'dark'
  });
  $mdThemingProvider.theme('default')
    .primaryPalette('amazingPaletteName')
}
