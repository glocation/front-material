export class MainController {
  constructor ( GAuth, $state) {
    'ngInject';
    this.GAuth = GAuth;
    this.state = $state;
  }

  login () {
    var GAuth = this.GAuth;
    var $state = this.state;
    GAuth.login().then(function (response) {
      console.log(response);
      $state.go('dashboard');
    });
  }
}
